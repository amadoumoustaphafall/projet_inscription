package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.AgentScolarite;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.AgentScolariteRepository;
import sn.esp.inscription.repository.search.AgentScolariteSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AgentScolariteResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class AgentScolariteResourceIT {

    private static final String DEFAULT_MATRICULE = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE = "BBBBBBBBBB";

    @Autowired
    private AgentScolariteRepository agentScolariteRepository;

    /**
     * This repository is mocked in the sn.esp.inscription.repository.search test package.
     *
     * @see sn.esp.inscription.repository.search.AgentScolariteSearchRepositoryMockConfiguration
     */
    @Autowired
    private AgentScolariteSearchRepository mockAgentScolariteSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAgentScolariteMockMvc;

    private AgentScolarite agentScolarite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentScolarite createEntity(EntityManager em) {
        AgentScolarite agentScolarite = new AgentScolarite()
            .matricule(DEFAULT_MATRICULE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        agentScolarite.setUser(user);
        return agentScolarite;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentScolarite createUpdatedEntity(EntityManager em) {
        AgentScolarite agentScolarite = new AgentScolarite()
            .matricule(UPDATED_MATRICULE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        agentScolarite.setUser(user);
        return agentScolarite;
    }

    @BeforeEach
    public void initTest() {
        agentScolarite = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgentScolarite() throws Exception {
        int databaseSizeBeforeCreate = agentScolariteRepository.findAll().size();
        // Create the AgentScolarite
        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isCreated());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeCreate + 1);
        AgentScolarite testAgentScolarite = agentScolariteList.get(agentScolariteList.size() - 1);
        assertThat(testAgentScolarite.getMatricule()).isEqualTo(DEFAULT_MATRICULE);

        // Validate the AgentScolarite in Elasticsearch
        verify(mockAgentScolariteSearchRepository, times(1)).save(testAgentScolarite);
    }

    @Test
    @Transactional
    public void createAgentScolariteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentScolariteRepository.findAll().size();

        // Create the AgentScolarite with an existing ID
        agentScolarite.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeCreate);

        // Validate the AgentScolarite in Elasticsearch
        verify(mockAgentScolariteSearchRepository, times(0)).save(agentScolarite);
    }


    @Test
    @Transactional
    public void checkMatriculeIsRequired() throws Exception {
        int databaseSizeBeforeTest = agentScolariteRepository.findAll().size();
        // set the field null
        agentScolarite.setMatricule(null);

        // Create the AgentScolarite, which fails.


        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAgentScolarites() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        // Get all the agentScolariteList
        restAgentScolariteMockMvc.perform(get("/api/agent-scolarites?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentScolarite.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)));
    }
    
    @Test
    @Transactional
    public void getAgentScolarite() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        // Get the agentScolarite
        restAgentScolariteMockMvc.perform(get("/api/agent-scolarites/{id}", agentScolarite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agentScolarite.getId().intValue()))
            .andExpect(jsonPath("$.matricule").value(DEFAULT_MATRICULE));
    }
    @Test
    @Transactional
    public void getNonExistingAgentScolarite() throws Exception {
        // Get the agentScolarite
        restAgentScolariteMockMvc.perform(get("/api/agent-scolarites/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgentScolarite() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        int databaseSizeBeforeUpdate = agentScolariteRepository.findAll().size();

        // Update the agentScolarite
        AgentScolarite updatedAgentScolarite = agentScolariteRepository.findById(agentScolarite.getId()).get();
        // Disconnect from session so that the updates on updatedAgentScolarite are not directly saved in db
        em.detach(updatedAgentScolarite);
        updatedAgentScolarite
            .matricule(UPDATED_MATRICULE);

        restAgentScolariteMockMvc.perform(put("/api/agent-scolarites").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgentScolarite)))
            .andExpect(status().isOk());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeUpdate);
        AgentScolarite testAgentScolarite = agentScolariteList.get(agentScolariteList.size() - 1);
        assertThat(testAgentScolarite.getMatricule()).isEqualTo(UPDATED_MATRICULE);

        // Validate the AgentScolarite in Elasticsearch
        verify(mockAgentScolariteSearchRepository, times(1)).save(testAgentScolarite);
    }

    @Test
    @Transactional
    public void updateNonExistingAgentScolarite() throws Exception {
        int databaseSizeBeforeUpdate = agentScolariteRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentScolariteMockMvc.perform(put("/api/agent-scolarites").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AgentScolarite in Elasticsearch
        verify(mockAgentScolariteSearchRepository, times(0)).save(agentScolarite);
    }

    @Test
    @Transactional
    public void deleteAgentScolarite() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        int databaseSizeBeforeDelete = agentScolariteRepository.findAll().size();

        // Delete the agentScolarite
        restAgentScolariteMockMvc.perform(delete("/api/agent-scolarites/{id}", agentScolarite.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the AgentScolarite in Elasticsearch
        verify(mockAgentScolariteSearchRepository, times(1)).deleteById(agentScolarite.getId());
    }

    @Test
    @Transactional
    public void searchAgentScolarite() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);
        when(mockAgentScolariteSearchRepository.search(queryStringQuery("id:" + agentScolarite.getId())))
            .thenReturn(Collections.singletonList(agentScolarite));

        // Search the agentScolarite
        restAgentScolariteMockMvc.perform(get("/api/_search/agent-scolarites?query=id:" + agentScolarite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentScolarite.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)));
    }
}
