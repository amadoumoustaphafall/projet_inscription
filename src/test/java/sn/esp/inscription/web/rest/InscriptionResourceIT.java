package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.search.InscriptionSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.esp.inscription.domain.enumeration.EnumSituation;
/**
 * Integration tests for the {@link InscriptionResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class InscriptionResourceIT {

    private static final Boolean DEFAULT_EST_APTE = false;
    private static final Boolean UPDATED_EST_APTE = true;

    private static final Boolean DEFAULT_EST_BOURSIER = false;
    private static final Boolean UPDATED_EST_BOURSIER = true;

    private static final Boolean DEFAULT_EST_ASSURE = false;
    private static final Boolean UPDATED_EST_ASSURE = true;

    private static final Boolean DEFAULT_EN_REGLE_BIBLIO = false;
    private static final Boolean UPDATED_EN_REGLE_BIBLIO = true;

    private static final EnumSituation DEFAULT_SITUATION_MATRIMONIALE = EnumSituation.Celibataire;
    private static final EnumSituation UPDATED_SITUATION_MATRIMONIALE = EnumSituation.MarieSansEnfant;

    @Autowired
    private InscriptionRepository inscriptionRepository;

    /**
     * This repository is mocked in the sn.esp.inscription.repository.search test package.
     *
     * @see sn.esp.inscription.repository.search.InscriptionSearchRepositoryMockConfiguration
     */
    @Autowired
    private InscriptionSearchRepository mockInscriptionSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInscriptionMockMvc;

    private Inscription inscription;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inscription createEntity(EntityManager em) {
        Inscription inscription = new Inscription()
            .estApte(DEFAULT_EST_APTE)
            .estBoursier(DEFAULT_EST_BOURSIER)
            .estAssure(DEFAULT_EST_ASSURE)
            .enRegleBiblio(DEFAULT_EN_REGLE_BIBLIO)
            .situationMatrimoniale(DEFAULT_SITUATION_MATRIMONIALE);
        return inscription;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inscription createUpdatedEntity(EntityManager em) {
        Inscription inscription = new Inscription()
            .estApte(UPDATED_EST_APTE)
            .estBoursier(UPDATED_EST_BOURSIER)
            .estAssure(UPDATED_EST_ASSURE)
            .enRegleBiblio(UPDATED_EN_REGLE_BIBLIO)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE);
        return inscription;
    }

    @BeforeEach
    public void initTest() {
        inscription = createEntity(em);
    }

    @Test
    @Transactional
    public void createInscription() throws Exception {
        int databaseSizeBeforeCreate = inscriptionRepository.findAll().size();
        // Create the Inscription
        restInscriptionMockMvc.perform(post("/api/inscriptions").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inscription)))
            .andExpect(status().isCreated());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeCreate + 1);
        Inscription testInscription = inscriptionList.get(inscriptionList.size() - 1);
        assertThat(testInscription.isEstApte()).isEqualTo(DEFAULT_EST_APTE);
        assertThat(testInscription.isEstBoursier()).isEqualTo(DEFAULT_EST_BOURSIER);
        assertThat(testInscription.isEstAssure()).isEqualTo(DEFAULT_EST_ASSURE);
        assertThat(testInscription.isEnRegleBiblio()).isEqualTo(DEFAULT_EN_REGLE_BIBLIO);
        assertThat(testInscription.getSituationMatrimoniale()).isEqualTo(DEFAULT_SITUATION_MATRIMONIALE);

        // Validate the Inscription in Elasticsearch
        verify(mockInscriptionSearchRepository, times(1)).save(testInscription);
    }

    @Test
    @Transactional
    public void createInscriptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inscriptionRepository.findAll().size();

        // Create the Inscription with an existing ID
        inscription.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInscriptionMockMvc.perform(post("/api/inscriptions").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inscription)))
            .andExpect(status().isBadRequest());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeCreate);

        // Validate the Inscription in Elasticsearch
        verify(mockInscriptionSearchRepository, times(0)).save(inscription);
    }


    @Test
    @Transactional
    public void checkSituationMatrimonialeIsRequired() throws Exception {
        int databaseSizeBeforeTest = inscriptionRepository.findAll().size();
        // set the field null
        inscription.setSituationMatrimoniale(null);

        // Create the Inscription, which fails.


        restInscriptionMockMvc.perform(post("/api/inscriptions").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inscription)))
            .andExpect(status().isBadRequest());

        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInscriptions() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        // Get all the inscriptionList
        restInscriptionMockMvc.perform(get("/api/inscriptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inscription.getId().intValue())))
            .andExpect(jsonPath("$.[*].estApte").value(hasItem(DEFAULT_EST_APTE.booleanValue())))
            .andExpect(jsonPath("$.[*].estBoursier").value(hasItem(DEFAULT_EST_BOURSIER.booleanValue())))
            .andExpect(jsonPath("$.[*].estAssure").value(hasItem(DEFAULT_EST_ASSURE.booleanValue())))
            .andExpect(jsonPath("$.[*].enRegleBiblio").value(hasItem(DEFAULT_EN_REGLE_BIBLIO.booleanValue())))
            .andExpect(jsonPath("$.[*].situationMatrimoniale").value(hasItem(DEFAULT_SITUATION_MATRIMONIALE.toString())));
    }
    
    @Test
    @Transactional
    public void getInscription() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        // Get the inscription
        restInscriptionMockMvc.perform(get("/api/inscriptions/{id}", inscription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(inscription.getId().intValue()))
            .andExpect(jsonPath("$.estApte").value(DEFAULT_EST_APTE.booleanValue()))
            .andExpect(jsonPath("$.estBoursier").value(DEFAULT_EST_BOURSIER.booleanValue()))
            .andExpect(jsonPath("$.estAssure").value(DEFAULT_EST_ASSURE.booleanValue()))
            .andExpect(jsonPath("$.enRegleBiblio").value(DEFAULT_EN_REGLE_BIBLIO.booleanValue()))
            .andExpect(jsonPath("$.situationMatrimoniale").value(DEFAULT_SITUATION_MATRIMONIALE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingInscription() throws Exception {
        // Get the inscription
        restInscriptionMockMvc.perform(get("/api/inscriptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInscription() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        int databaseSizeBeforeUpdate = inscriptionRepository.findAll().size();

        // Update the inscription
        Inscription updatedInscription = inscriptionRepository.findById(inscription.getId()).get();
        // Disconnect from session so that the updates on updatedInscription are not directly saved in db
        em.detach(updatedInscription);
        updatedInscription
            .estApte(UPDATED_EST_APTE)
            .estBoursier(UPDATED_EST_BOURSIER)
            .estAssure(UPDATED_EST_ASSURE)
            .enRegleBiblio(UPDATED_EN_REGLE_BIBLIO)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE);

        restInscriptionMockMvc.perform(put("/api/inscriptions").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedInscription)))
            .andExpect(status().isOk());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeUpdate);
        Inscription testInscription = inscriptionList.get(inscriptionList.size() - 1);
        assertThat(testInscription.isEstApte()).isEqualTo(UPDATED_EST_APTE);
        assertThat(testInscription.isEstBoursier()).isEqualTo(UPDATED_EST_BOURSIER);
        assertThat(testInscription.isEstAssure()).isEqualTo(UPDATED_EST_ASSURE);
        assertThat(testInscription.isEnRegleBiblio()).isEqualTo(UPDATED_EN_REGLE_BIBLIO);
        assertThat(testInscription.getSituationMatrimoniale()).isEqualTo(UPDATED_SITUATION_MATRIMONIALE);

        // Validate the Inscription in Elasticsearch
        verify(mockInscriptionSearchRepository, times(1)).save(testInscription);
    }

    @Test
    @Transactional
    public void updateNonExistingInscription() throws Exception {
        int databaseSizeBeforeUpdate = inscriptionRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInscriptionMockMvc.perform(put("/api/inscriptions").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inscription)))
            .andExpect(status().isBadRequest());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Inscription in Elasticsearch
        verify(mockInscriptionSearchRepository, times(0)).save(inscription);
    }

    @Test
    @Transactional
    public void deleteInscription() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        int databaseSizeBeforeDelete = inscriptionRepository.findAll().size();

        // Delete the inscription
        restInscriptionMockMvc.perform(delete("/api/inscriptions/{id}", inscription.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Inscription in Elasticsearch
        verify(mockInscriptionSearchRepository, times(1)).deleteById(inscription.getId());
    }

    @Test
    @Transactional
    public void searchInscription() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);
        when(mockInscriptionSearchRepository.search(queryStringQuery("id:" + inscription.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(inscription), PageRequest.of(0, 1), 1));

        // Search the inscription
        restInscriptionMockMvc.perform(get("/api/_search/inscriptions?query=id:" + inscription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inscription.getId().intValue())))
            .andExpect(jsonPath("$.[*].estApte").value(hasItem(DEFAULT_EST_APTE.booleanValue())))
            .andExpect(jsonPath("$.[*].estBoursier").value(hasItem(DEFAULT_EST_BOURSIER.booleanValue())))
            .andExpect(jsonPath("$.[*].estAssure").value(hasItem(DEFAULT_EST_ASSURE.booleanValue())))
            .andExpect(jsonPath("$.[*].enRegleBiblio").value(hasItem(DEFAULT_EN_REGLE_BIBLIO.booleanValue())))
            .andExpect(jsonPath("$.[*].situationMatrimoniale").value(hasItem(DEFAULT_SITUATION_MATRIMONIALE.toString())));
    }
}
