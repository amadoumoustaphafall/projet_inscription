package sn.esp.inscription.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link AssureurSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class AssureurSearchRepositoryMockConfiguration {

    @MockBean
    private AssureurSearchRepository mockAssureurSearchRepository;

}
