import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';

type EntityResponseType = HttpResponse<IAgentScolarite>;
type EntityArrayResponseType = HttpResponse<IAgentScolarite[]>;

@Injectable({ providedIn: 'root' })
export class AgentScolariteService {
  public resourceUrl = SERVER_API_URL + 'api/agent-scolarites';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/agent-scolarites';

  constructor(protected http: HttpClient) {}

  create(agentScolarite: IAgentScolarite): Observable<EntityResponseType> {
    return this.http.post<IAgentScolarite>(this.resourceUrl, agentScolarite, { observe: 'response' });
  }

  update(agentScolarite: IAgentScolarite): Observable<EntityResponseType> {
    return this.http.put<IAgentScolarite>(this.resourceUrl, agentScolarite, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAgentScolarite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAgentScolarite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAgentScolarite[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
