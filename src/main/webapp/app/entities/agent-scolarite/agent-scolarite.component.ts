import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';
import { AgentScolariteService } from './agent-scolarite.service';
import { AgentScolariteDeleteDialogComponent } from './agent-scolarite-delete-dialog.component';

@Component({
  selector: 'jhi-agent-scolarite',
  templateUrl: './agent-scolarite.component.html',
})
export class AgentScolariteComponent implements OnInit, OnDestroy {
  agentScolarites?: IAgentScolarite[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected agentScolariteService: AgentScolariteService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.agentScolariteService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IAgentScolarite[]>) => (this.agentScolarites = res.body || []));
      return;
    }

    this.agentScolariteService.query().subscribe((res: HttpResponse<IAgentScolarite[]>) => (this.agentScolarites = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAgentScolarites();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAgentScolarite): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAgentScolarites(): void {
    this.eventSubscriber = this.eventManager.subscribe('agentScolariteListModification', () => this.loadAll());
  }

  delete(agentScolarite: IAgentScolarite): void {
    const modalRef = this.modalService.open(AgentScolariteDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.agentScolarite = agentScolarite;
  }
}
