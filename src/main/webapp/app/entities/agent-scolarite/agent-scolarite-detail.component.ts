import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';

@Component({
  selector: 'jhi-agent-scolarite-detail',
  templateUrl: './agent-scolarite-detail.component.html',
})
export class AgentScolariteDetailComponent implements OnInit {
  agentScolarite: IAgentScolarite | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentScolarite }) => (this.agentScolarite = agentScolarite));
  }

  previousState(): void {
    window.history.back();
  }
}
