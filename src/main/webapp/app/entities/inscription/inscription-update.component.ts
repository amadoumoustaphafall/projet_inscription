import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IInscription, Inscription } from 'app/shared/model/inscription.model';
import { InscriptionService } from './inscription.service';
import { INiveau } from 'app/shared/model/niveau.model';
import { NiveauService } from 'app/entities/niveau/niveau.service';
import { IAnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';
import { AnneeUniversitaireService } from 'app/entities/annee-universitaire/annee-universitaire.service';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';

type SelectableEntity = INiveau | IAnneeUniversitaire | IEtudiant;

@Component({
  selector: 'jhi-inscription-update',
  templateUrl: './inscription-update.component.html',
})
export class InscriptionUpdateComponent implements OnInit {
  isSaving = false;
  niveaus: INiveau[] = [];
  anneeuniversitaires: IAnneeUniversitaire[] = [];
  etudiants: IEtudiant[] = [];

  editForm = this.fb.group({
    id: [],
    estApte: [],
    estBoursier: [],
    estAssure: [],
    enRegleBiblio: [],
    situationMatrimoniale: [null, [Validators.required]],
    niveau: [],
    anneeUniversitaire: [],
    etudiant: [],
  });

  constructor(
    protected inscriptionService: InscriptionService,
    protected niveauService: NiveauService,
    protected anneeUniversitaireService: AnneeUniversitaireService,
    protected etudiantService: EtudiantService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ inscription }) => {
      this.updateForm(inscription);

      this.niveauService.query().subscribe((res: HttpResponse<INiveau[]>) => (this.niveaus = res.body || []));

      this.anneeUniversitaireService
        .query()
        .subscribe((res: HttpResponse<IAnneeUniversitaire[]>) => (this.anneeuniversitaires = res.body || []));

      this.etudiantService.query().subscribe((res: HttpResponse<IEtudiant[]>) => (this.etudiants = res.body || []));
    });
  }

  updateForm(inscription: IInscription): void {
    this.editForm.patchValue({
      id: inscription.id,
      estApte: inscription.estApte,
      estBoursier: inscription.estBoursier,
      estAssure: inscription.estAssure,
      enRegleBiblio: inscription.enRegleBiblio,
      situationMatrimoniale: inscription.situationMatrimoniale,
      niveau: inscription.niveau,
      anneeUniversitaire: inscription.anneeUniversitaire,
      etudiant: inscription.etudiant,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const inscription = this.createFromForm();
    if (inscription.id !== undefined) {
      this.subscribeToSaveResponse(this.inscriptionService.update(inscription));
    } else {
      this.subscribeToSaveResponse(this.inscriptionService.create(inscription));
    }
  }

  private createFromForm(): IInscription {
    return {
      ...new Inscription(),
      id: this.editForm.get(['id'])!.value,
      estApte: this.editForm.get(['estApte'])!.value,
      estBoursier: this.editForm.get(['estBoursier'])!.value,
      estAssure: this.editForm.get(['estAssure'])!.value,
      enRegleBiblio: this.editForm.get(['enRegleBiblio'])!.value,
      situationMatrimoniale: this.editForm.get(['situationMatrimoniale'])!.value,
      niveau: this.editForm.get(['niveau'])!.value,
      anneeUniversitaire: this.editForm.get(['anneeUniversitaire'])!.value,
      etudiant: this.editForm.get(['etudiant'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInscription>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
