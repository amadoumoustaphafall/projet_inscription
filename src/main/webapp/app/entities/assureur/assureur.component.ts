import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssureur } from 'app/shared/model/assureur.model';
import { AssureurService } from './assureur.service';
import { AssureurDeleteDialogComponent } from './assureur-delete-dialog.component';

@Component({
  selector: 'jhi-assureur',
  templateUrl: './assureur.component.html',
})
export class AssureurComponent implements OnInit, OnDestroy {
  assureurs?: IAssureur[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected assureurService: AssureurService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.assureurService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IAssureur[]>) => (this.assureurs = res.body || []));
      return;
    }

    this.assureurService.query().subscribe((res: HttpResponse<IAssureur[]>) => (this.assureurs = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAssureurs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAssureur): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAssureurs(): void {
    this.eventSubscriber = this.eventManager.subscribe('assureurListModification', () => this.loadAll());
  }

  delete(assureur: IAssureur): void {
    const modalRef = this.modalService.open(AssureurDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.assureur = assureur;
  }
}
