import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { IAnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';

type EntityResponseType = HttpResponse<IAnneeUniversitaire>;
type EntityArrayResponseType = HttpResponse<IAnneeUniversitaire[]>;

@Injectable({ providedIn: 'root' })
export class AnneeUniversitaireService {
  public resourceUrl = SERVER_API_URL + 'api/annee-universitaires';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/annee-universitaires';

  constructor(protected http: HttpClient) {}

  create(anneeUniversitaire: IAnneeUniversitaire): Observable<EntityResponseType> {
    return this.http.post<IAnneeUniversitaire>(this.resourceUrl, anneeUniversitaire, { observe: 'response' });
  }

  update(anneeUniversitaire: IAnneeUniversitaire): Observable<EntityResponseType> {
    return this.http.put<IAnneeUniversitaire>(this.resourceUrl, anneeUniversitaire, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAnneeUniversitaire>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAnneeUniversitaire[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAnneeUniversitaire[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
