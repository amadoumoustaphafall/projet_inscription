import { IDepartement } from 'app/shared/model/departement.model';
import { INiveau } from 'app/shared/model/niveau.model';

export interface IFormation {
  id?: number;
  codeFormation?: string;
  libelleLong?: string;
  departement?: IDepartement;
  formations?: INiveau[];
}

export class Formation implements IFormation {
  constructor(
    public id?: number,
    public codeFormation?: string,
    public libelleLong?: string,
    public departement?: IDepartement,
    public formations?: INiveau[]
  ) {}
}
