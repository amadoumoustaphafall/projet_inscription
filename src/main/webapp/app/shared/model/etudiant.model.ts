import { Moment } from 'moment';
import { IInscription } from 'app/shared/model/inscription.model';
import { IUser } from 'app/core/user/user.model';
import { EnumSexe } from 'app/shared/model/enumerations/enum-sexe.model';

export interface IEtudiant {
  id?: number;
  numIdentifiant?: string;
  dateNaissance?: Moment;
  lieuNaissance?: string;
  sexe?: EnumSexe;
  ine?: string;
  telephone?: string;
  etudiants?: IInscription[];
  user?: IUser;
}

export class Etudiant implements IEtudiant {
  constructor(
    public id?: number,
    public numIdentifiant?: string,
    public dateNaissance?: Moment,
    public lieuNaissance?: string,
    public sexe?: EnumSexe,
    public ine?: string,
    public telephone?: string,
    public etudiants?: IInscription[],
    public user?: IUser
  ) {}
}
