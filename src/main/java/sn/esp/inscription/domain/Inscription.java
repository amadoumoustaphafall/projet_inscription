package sn.esp.inscription.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

import sn.esp.inscription.domain.enumeration.EnumSituation;

/**
 * A Inscription.
 */
@Entity
@Table(name = "inscription")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "inscription")
public class Inscription implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "est_apte")
    private Boolean estApte;

    @Column(name = "est_boursier")
    private Boolean estBoursier;

    @Column(name = "est_assure")
    private Boolean estAssure;

    @Column(name = "en_regle_biblio")
    private Boolean enRegleBiblio;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "situation_matrimoniale", nullable = false)
    private EnumSituation situationMatrimoniale;

    @ManyToOne
    @JsonIgnoreProperties(value = "niveaus", allowSetters = true)
    private Niveau niveau;

    @ManyToOne
    @JsonIgnoreProperties(value = "anneeUniversitaires", allowSetters = true)
    private AnneeUniversitaire anneeUniversitaire;

    @ManyToOne
    @JsonIgnoreProperties(value = "etudiants", allowSetters = true)
    private Etudiant etudiant;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isEstApte() {
        return estApte;
    }

    public Inscription estApte(Boolean estApte) {
        this.estApte = estApte;
        return this;
    }

    public void setEstApte(Boolean estApte) {
        this.estApte = estApte;
    }

    public Boolean isEstBoursier() {
        return estBoursier;
    }

    public Inscription estBoursier(Boolean estBoursier) {
        this.estBoursier = estBoursier;
        return this;
    }

    public void setEstBoursier(Boolean estBoursier) {
        this.estBoursier = estBoursier;
    }

    public Boolean isEstAssure() {
        return estAssure;
    }

    public Inscription estAssure(Boolean estAssure) {
        this.estAssure = estAssure;
        return this;
    }

    public void setEstAssure(Boolean estAssure) {
        this.estAssure = estAssure;
    }

    public Boolean isEnRegleBiblio() {
        return enRegleBiblio;
    }

    public Inscription enRegleBiblio(Boolean enRegleBiblio) {
        this.enRegleBiblio = enRegleBiblio;
        return this;
    }

    public void setEnRegleBiblio(Boolean enRegleBiblio) {
        this.enRegleBiblio = enRegleBiblio;
    }

    public EnumSituation getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public Inscription situationMatrimoniale(EnumSituation situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
        return this;
    }

    public void setSituationMatrimoniale(EnumSituation situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public Niveau getNiveau() {
        return niveau;
    }

    public Inscription niveau(Niveau niveau) {
        this.niveau = niveau;
        return this;
    }

    public void setNiveau(Niveau niveau) {
        this.niveau = niveau;
    }

    public AnneeUniversitaire getAnneeUniversitaire() {
        return anneeUniversitaire;
    }

    public Inscription anneeUniversitaire(AnneeUniversitaire anneeUniversitaire) {
        this.anneeUniversitaire = anneeUniversitaire;
        return this;
    }

    public void setAnneeUniversitaire(AnneeUniversitaire anneeUniversitaire) {
        this.anneeUniversitaire = anneeUniversitaire;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public Inscription etudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
        return this;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Inscription)) {
            return false;
        }
        return id != null && id.equals(((Inscription) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Inscription{" +
            "id=" + getId() +
            ", estApte='" + isEstApte() + "'" +
            ", estBoursier='" + isEstBoursier() + "'" +
            ", estAssure='" + isEstAssure() + "'" +
            ", enRegleBiblio='" + isEnRegleBiblio() + "'" +
            ", situationMatrimoniale='" + getSituationMatrimoniale() + "'" +
            "}";
    }
}
