package sn.esp.inscription.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Niveau.
 */
@Entity
@Table(name = "niveau")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "niveau")
public class Niveau implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 3)
    @Column(name = "code_niveau", nullable = false, unique = true)
    private String codeNiveau;

    @NotNull
    @Column(name = "libelle_long", nullable = false)
    private String libelleLong;

    @ManyToOne
    @JsonIgnoreProperties(value = "formations", allowSetters = true)
    private Formation formation;

    @OneToMany(mappedBy = "niveau")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Inscription> niveaus = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeNiveau() {
        return codeNiveau;
    }

    public Niveau codeNiveau(String codeNiveau) {
        this.codeNiveau = codeNiveau;
        return this;
    }

    public void setCodeNiveau(String codeNiveau) {
        this.codeNiveau = codeNiveau;
    }

    public String getLibelleLong() {
        return libelleLong;
    }

    public Niveau libelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
        return this;
    }

    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    public Formation getFormation() {
        return formation;
    }

    public Niveau formation(Formation formation) {
        this.formation = formation;
        return this;
    }

    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    public Set<Inscription> getNiveaus() {
        return niveaus;
    }

    public Niveau niveaus(Set<Inscription> inscriptions) {
        this.niveaus = inscriptions;
        return this;
    }

    public Niveau addNiveau(Inscription inscription) {
        this.niveaus.add(inscription);
        inscription.setNiveau(this);
        return this;
    }

    public Niveau removeNiveau(Inscription inscription) {
        this.niveaus.remove(inscription);
        inscription.setNiveau(null);
        return this;
    }

    public void setNiveaus(Set<Inscription> inscriptions) {
        this.niveaus = inscriptions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Niveau)) {
            return false;
        }
        return id != null && id.equals(((Niveau) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Niveau{" +
            "id=" + getId() +
            ", codeNiveau='" + getCodeNiveau() + "'" +
            ", libelleLong='" + getLibelleLong() + "'" +
            "}";
    }
}
