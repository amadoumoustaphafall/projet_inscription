package sn.esp.inscription.web.rest;

import sn.esp.inscription.domain.AgentBiblio;
import sn.esp.inscription.repository.AgentBiblioRepository;
import sn.esp.inscription.repository.search.AgentBiblioSearchRepository;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.AgentBiblio}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AgentBiblioResource {

    private final Logger log = LoggerFactory.getLogger(AgentBiblioResource.class);

    private static final String ENTITY_NAME = "agentBiblio";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentBiblioRepository agentBiblioRepository;

    private final AgentBiblioSearchRepository agentBiblioSearchRepository;

    public AgentBiblioResource(AgentBiblioRepository agentBiblioRepository, AgentBiblioSearchRepository agentBiblioSearchRepository) {
        this.agentBiblioRepository = agentBiblioRepository;
        this.agentBiblioSearchRepository = agentBiblioSearchRepository;
    }

    /**
     * {@code POST  /agent-biblios} : Create a new agentBiblio.
     *
     * @param agentBiblio the agentBiblio to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentBiblio, or with status {@code 400 (Bad Request)} if the agentBiblio has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-biblios")
    public ResponseEntity<AgentBiblio> createAgentBiblio(@Valid @RequestBody AgentBiblio agentBiblio) throws URISyntaxException {
        log.debug("REST request to save AgentBiblio : {}", agentBiblio);
        if (agentBiblio.getId() != null) {
            throw new BadRequestAlertException("A new agentBiblio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentBiblio result = agentBiblioRepository.save(agentBiblio);
        agentBiblioSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/agent-biblios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-biblios} : Updates an existing agentBiblio.
     *
     * @param agentBiblio the agentBiblio to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentBiblio,
     * or with status {@code 400 (Bad Request)} if the agentBiblio is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentBiblio couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-biblios")
    public ResponseEntity<AgentBiblio> updateAgentBiblio(@Valid @RequestBody AgentBiblio agentBiblio) throws URISyntaxException {
        log.debug("REST request to update AgentBiblio : {}", agentBiblio);
        if (agentBiblio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentBiblio result = agentBiblioRepository.save(agentBiblio);
        agentBiblioSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agentBiblio.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agent-biblios} : get all the agentBiblios.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentBiblios in body.
     */
    @GetMapping("/agent-biblios")
    public List<AgentBiblio> getAllAgentBiblios() {
        log.debug("REST request to get all AgentBiblios");
        return agentBiblioRepository.findAll();
    }

    /**
     * {@code GET  /agent-biblios/:id} : get the "id" agentBiblio.
     *
     * @param id the id of the agentBiblio to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentBiblio, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-biblios/{id}")
    public ResponseEntity<AgentBiblio> getAgentBiblio(@PathVariable Long id) {
        log.debug("REST request to get AgentBiblio : {}", id);
        Optional<AgentBiblio> agentBiblio = agentBiblioRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentBiblio);
    }

    /**
     * {@code DELETE  /agent-biblios/:id} : delete the "id" agentBiblio.
     *
     * @param id the id of the agentBiblio to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-biblios/{id}")
    public ResponseEntity<Void> deleteAgentBiblio(@PathVariable Long id) {
        log.debug("REST request to delete AgentBiblio : {}", id);
        agentBiblioRepository.deleteById(id);
        agentBiblioSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/agent-biblios?query=:query} : search for the agentBiblio corresponding
     * to the query.
     *
     * @param query the query of the agentBiblio search.
     * @return the result of the search.
     */
    @GetMapping("/_search/agent-biblios")
    public List<AgentBiblio> searchAgentBiblios(@RequestParam String query) {
        log.debug("REST request to search AgentBiblios for query {}", query);
        return StreamSupport
            .stream(agentBiblioSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
