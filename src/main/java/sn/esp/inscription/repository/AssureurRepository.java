package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Assureur;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Assureur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssureurRepository extends JpaRepository<Assureur, Long> {
}
