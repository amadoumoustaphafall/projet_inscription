package sn.esp.inscription.repository.search;

import sn.esp.inscription.domain.AgentBiblio;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link AgentBiblio} entity.
 */
public interface AgentBiblioSearchRepository extends ElasticsearchRepository<AgentBiblio, Long> {
}
